# Source of Mana

Client and Game development for Source of Mana, a The Mana World story.

## Tools

Godot current version is ['Godot 4.0.1'](https://github.com/godotengine/godot/releases/download/4.0.1-stable/Godot_v4.0.1-stable_win64.exe.zip) accessible on [Godot's official website](https://godotengine.org/download).
Tiled current version is ['Tiled 1.9'](https://www.mapeditor.org/2022/06/25/tiled-1-9-released.html) accessible on [Tiled's official website](https://www.mapeditor.org/).

## License

This project is distributed under the terms of the MIT license, this includes every part of this repository except the data folder that is released under the CC BY-SA 4.0 license.
You can find describtion of such licenses  in the [LICENSE.md](LICENSE.md) file.
